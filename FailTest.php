<?php


require_once 'API.php';
use PHPUnit\Framework\TestCase;
class APIFailTest extends TestCase
{
    public $api;
    protected function setUp(): void
    {
        $this->api = new API();
    }
    
    public function testPayload()
    {
    $payload = array(
        'id' => 123123,
        'first_name' => 'evin',
        'middle_name' => '',
        'last_name' => 'cuenco',
        'contact_number' => 1231231
    );
        //ensure payload is valid and array
        $this->assertIsArray($payload);
        $this->assertArrayHasKey('id', $payload);
        $this->assertArrayHasKey('first_name', $payload);
        $this->assertArrayHasKey('middle_name', $payload);
        $this->assertArrayHasKey('last_name', $payload);
        $this->assertArrayHasKey('contact_number', $payload);
        $this->assertNotEmpty($payload);
    return $payload;
    }

    /**
    * @depends testPayload 
    */
    public function testHttpPostFail($payload)
    {
    //request method to post to stimulate request
    $_SERVER['REQUEST_METHOD'] = 'POST';
    //call httppost function
    $result = json_decode($this->api->httpPost($payload), true);
    
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('data', $result); ;
        $this->assertArrayHasKey('message', $result);
        $this->assertNotEmpty($result['message']);
    }

    /**
    * @depends testPayload
    */
    public function testHttpGetFail($payload)
    {
    $_SERVER['REQUEST_METHOD'] = 'GET';
    $result = json_decode($this->api->httpGet(['id' => $payload['id']]), true);
   
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);
    }

    /**
    *@depends testPayload
    */
    public function testHttpPutFail($payload)
    {
    $_SERVER['REQUEST_METHOD'] = 'PUT';
    $id = $payload['id'];
    $payload = [
        'id' => $id,
        'first_name' => 'hello',
        'last_name' => 'hello',
        'contact_number' => '1234567890',
    ];
    $result = json_decode($this->api->httpPut($id, $payload), true);
    
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);
    }

    /**
    * @depends testPayload
    */
    public function testHttpDeleteFail($payload)
    {
    $_SERVER['REQUEST_METHOD'] = 'DELETE';
    $id = $payload['id'];
    $result = json_decode($this->api->httpDelete(['id' => $id]), true); 
   
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);
    }
}?>