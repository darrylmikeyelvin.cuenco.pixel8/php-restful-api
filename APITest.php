<?php


require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    public $api;
    protected function setUp(): void
    {
        $this->api = new API();
    }
    
    public function testPayload()
    {
    $payload = array(
        'id' => 1,
        'first_name' => 'hello',
        'middle_name' => 'elvin',
        'last_name' => 'cuenco',
        'contact_number' => 12345
    );
        //ensure payload is valid 
        $this->assertIsArray($payload);
        $this->assertArrayHasKey('id', $payload);
        $this->assertArrayHasKey('first_name', $payload);
        $this->assertArrayHasKey('middle_name', $payload);
        $this->assertArrayHasKey('last_name', $payload);
        $this->assertArrayHasKey('contact_number', $payload);
        $this->assertNotEmpty($payload);

    return $payload;
    }

    /**
    *@depends testPayload
    */ 
    public function testHttpPost($payload)
    {
    $_SERVER['REQUEST_METHOD'] = 'POST';

    $result = json_decode($this->api->httpPost($payload), true);

    $this->assertArrayHasKey('status', $result);
    $this->assertEquals($result['status'], 'success');
    $this->assertArrayHasKey('data', $result); 
    $this->assertIsArray($result['data']);
    $this->assertNotEmpty($result);
    }
    
    /**
    *@depends testPayload
    */ 
    public function testHttpGet($payload)
    {
    if (!$payload) {
        $payload = array('id' => 1);
    }

    $_SERVER['REQUEST_METHOD'] = 'GET';
    $id_exists = $payload['id'];
    $result = json_decode($this->api->httpGet(['id' => $id_exists]), true);

    $this->assertArrayHasKey('status', $result);
    $this->assertEquals($result['status'], 'success');
    $this->assertArrayHasKey('data', $result);
    $this->assertNotEmpty($result['data']);
    $this->assertIsArray($result['data']);
    }
      
    /**
    * @depends testPayload
    */
    public function testHttpPut($payload)
    {
    $_SERVER['REQUEST_METHOD'] = 'PUT';
    $id = $payload['id'];

    $response = json_decode($this->api->httpPut($id, $payload), true);
      
    $this->assertArrayHasKey('id', $response['data']);
    $this->assertArrayHasKey('data', $response);
    $this->assertIsArray($response['data']);
    $this->assertNotEmpty($response['data']);
    }
  
    /**
    * @depends testPayload
    */
    public function testHttpDelete($payload)
    {
    $_SERVER['REQUEST_METHOD'] = 'DELETE';    
      
    $id = $payload['id'];
    
    $response = json_decode($this->api->httpDelete(['id' => $id]), true);
    
    $this->assertArrayHasKey('status', $response);
    $this->assertArrayHasKey('data', $response);
    $this->assertArrayHasKey('id', $response['data']);
    $this->assertIsArray($response['data']);
    $this->assertNotEmpty($response['data']);
    } 
}
