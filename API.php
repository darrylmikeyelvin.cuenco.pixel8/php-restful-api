<?php  
/**
   * Tells the browser to allow code from any origin to access
   */
  header("Access-Control-Allow-Origin: *");

  /**
   * Tells browsers whether to expose the response to the frontend JavaScript code
   * when the request's credentials mode (Request.credentials) is include
   */
  header("Access-Control-Allow-Credentials: true");
 
  /**
   * Specifies one or more methods allowed when accessing a resource in response to a preflight request
   */
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
  /**
   * Used in response to a preflight request which includes the Access-Control-Request-Headers to
   * indicate which HTTP headers can be used during the actual request
   */

  header("Access-Control-Allow-Headers: Content-Type");
 //need to comment because phpunit test considered
  //require_once ('MysqliDb.php');
  class API 
{
    public $db;
    public function __construct()
    {
        $this-> db = new MysqliDb('localhost', 'root', '', 'employee');
    }
    //HTTP GET REQUEST
    public function httpGet($received_data){    
        if (!isset($received_data['id'])) {
            // if no id show all information
            $query = $this->db->get('information');
        } else {
            // proceed with fetching data for the provided id
            $id = $received_data['id'];
            $query = $this->db->where('id', $id)->getOne('information');
        }

        if ($query) {

            $response = array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $query,
                );
        } else {

            $response = array(
                'method' => 'GET',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to fetch data: record not found');
            }
        return json_encode($response);
    }

    //HTTP POST REQUEST
    public function httpPost($payload){
            //check if first name middle name and last name has an input
        if(empty($payload['first_name']) || empty($payload['middle_name'])  || empty($payload['last_name']) || empty($payload['contact_number'])){

            $response = array(
                'method' => 'POST',
                'status' => 'fail',
                'data' => [],
                'message' => 'inputs cannot be empty');
            return json_encode($response); 
            }
            //Execute Query
            $query = $this->db->insert('information', $payload);
            //check if query is success or fail
        if ($query) {

            $response  = array(
                'method' => 'POST',
                'status' => 'success',
                'data' => $payload,);
            } else {
            $response  = array(
                'method' => 'POST',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Insert');
            }
        return json_encode($response);  
    }

        //HTTP PUT REQUEST
    function httpPut($id, $payload){
        //check if name and contact is in the input
        if(empty($payload['first_name']) || empty($payload['middle_name']) || empty($payload['last_name']) || empty($payload['contact_number'])){

            $response = array(
                'method' => 'POST',
                'status' => 'fail',
                'data' => [],
                'message' => 'input fields cannot be empty');

            return json_encode($response);  
        }
        // where clause
        $query = $this->db->where('id', $payload['id'])->get('information');
        //check if query is success or fail
        if ($query) {
            $this->db->where('id', $payload['id'])->update('information', $payload);

                $response = array(
                        'method' => 'PUT',
                        'status' => 'success',
                        'data' => $payload,);
            } else {

                $response = array(
                        'method' => 'PUT',
                        'status' => 'fail',
                        'data' => [],
                        'message' => 'Failed to Update no id in the database');
                    }
                return json_encode($response);
            }
        

        
    public function httpDelete($received_data) {
        //check id
        if (!isset($received_data['id'])) {
            $response = array(
                'method' => 'DELETE',
                'status' => 'fail',
                'data' => [],
                'message' => 'ID not in database',);
            return json_encode($response);
            }else{          
            $id = $received_data['id'];
            $query = $this->db->where('id', $id)->getOne('information');

        if ($query) {
            $deletedRecord = $query;
            $this->db->where('id', $id)->delete('information');
            $response = array(
                    'method' => 'DELETE',
                    'status' => 'success',
                    'data' => $deletedRecord,
                    'message' => 'Record deleted successfully',
                );
            }else{
            $response = array(
                    'method' => 'DELETE',
                    'status' => 'fail',
                    'data' => [],
                    'message' => 'Invalid ID',);
                }
            return json_encode($response);
        }
    }
}

    $id = null;
    if(isset($_SERVER['REQUEST_METHOD'])){
        $request_method = $_SERVER['REQUEST_METHOD'];
        if ($request_method === 'GET') {
           $received_data = json_decode(file_get_contents('php://input'), true);
        }else{
            //check if method is PUT or DELETE, and get the ids on URL
            if ($request_method === 'PUT') {
                $request_uri = $_SERVER['REQUEST_URI'];
                $pattern = "/api\.php\/(\d+)/"; // matches "api.php/{ID}"
                preg_match($pattern, $request_uri, $matches);
                $id = isset($matches[1]) ? $matches[1] : null;
            
            }else if ($request_method === 'DELETE'){
                $request_uri = $_SERVER['REQUEST_URI'];
                $pattern = "/api\.php\/(\d+)/"; // matches "api.php/{ID}"
                preg_match($pattern, $request_uri, $matches);
                $id = isset($matches[1]) ? $matches[1] : null;
                $received_data = [];
                $received_data['id'] = $id;
            }
            //payload data
            $received_data = json_decode(file_get_contents('php://input'), true);
        }

    }else{
        $request_method = $_SERVER['REQUEST_METHOD'];
}


    $api = new API;
    //Checking if what type of request and designating to specific functions
    switch ($request_method) {
        case 'GET':
            $api->httpGet($received_data);
            break;
        case 'POST':
            $api->httpPost($received_data);
            break;
        case 'PUT':
            $api->httpPut($id, $received_data);
            break;
        case 'DELETE':
            $api->httpDelete($received_data);
            break;
    }
?>

